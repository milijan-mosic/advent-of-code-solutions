package main

import (
	utils "aoc"
)

func calcPoint(char string) int {
	ascii := int(char[0])

	if ascii >= 65 && ascii <= 90 {
		return ascii - 38
	} else {
		return ascii - 96
	}
}

func main() {
	rucksacks_input := utils.ReadFile("./three_input.txt")

	main_input_len := len(rucksacks_input)
	items_first_compartment := make([]string, 0)
	items_second_compartment := make([]string, 0)

	// **Prep data for calculation**
	for i := 0; i < main_input_len; i++ {
		items := utils.Split(rucksacks_input[i], "")
		items_len := len(items)
		temp_first_compartment := ""
		temp_second_compartment := ""

		for j := 0; j < items_len; j++ {
			if j < items_len/2 {
				temp_first_compartment += items[j]
			} else {
				temp_second_compartment += items[j]
			}
		}

		items_first_compartment = append(items_first_compartment, temp_first_compartment)
		items_second_compartment = append(items_second_compartment, temp_second_compartment)
	}

	// PART 1
	item_sum := 0
	for i := 0; i < len(items_first_compartment)-1; i++ {
		first_items := utils.Split(items_first_compartment[i], "")
		second_items := utils.Split(items_second_compartment[i], "")
		common_item := ""

		for x := 0; x < len(first_items); x++ {
			for y := 0; y < len(second_items); y++ {
				if first_items[x] == second_items[y] {
					common_item = first_items[x]
				}
			}
		}

		item_sum += calcPoint(common_item)
	}
	utils.Print("Item sum: ", item_sum)

	// PART 2
	badge_sum := 0
	j := 0

	for i := 0; i < main_input_len/3; i++ {
		first_group := utils.Split(rucksacks_input[j], "")
		second_group := utils.Split(rucksacks_input[j+1], "")
		third_group := utils.Split(rucksacks_input[j+2], "")
		common_badge := ""

		for x := 0; x < len(first_group); x++ {
			for y := 0; y < len(second_group); y++ {
				for z := 0; z < len(third_group); z++ {
					if first_group[x] == second_group[y] && second_group[y] == third_group[z] {
						common_badge = first_group[x]
					}
				}
			}
		}

		badge_sum += calcPoint(common_badge)
		j += 3
	}
	utils.Print("Badge sum: ", badge_sum)
}
