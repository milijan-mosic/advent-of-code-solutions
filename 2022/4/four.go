package main

import (
	utils "aoc"
)

func containsSection(first, second []string) bool {
	first_from := utils.StringToInt(first[0])
	first_to := utils.StringToInt(first[1])
	second_from := utils.StringToInt(second[0])
	second_to := utils.StringToInt(second[1])

	if first_from <= second_from && first_to >= second_to {
		return true
	}
	return false
}

func overlapsSection(first, second []string) bool {
	first_from := utils.StringToInt(first[0])
	first_to := utils.StringToInt(first[1])
	second_from := utils.StringToInt(second[0])
	second_to := utils.StringToInt(second[1])

	if first_to == second_from {
		return true
	}

	if second_to == first_from {
		return true
	}

	if second_from >= first_from && second_to <= first_to {
		return true
	}

	if (second_from >= first_from && second_from <= first_to) && (first_to >= second_from && first_to <= second_to) {
		return true
	}

	return false
}

func main() {
	elfPairsRaw := utils.ReadFile("./four_input.txt")

	numOfIdenticalSection := 0
	numOfOverlappingSection := 0
	for i := 0; i < len(elfPairsRaw)-1; i++ {
		pair := utils.Split(elfPairsRaw[i], ",")

		firstPair := utils.Split(pair[0], "-")
		secondPair := utils.Split(pair[1], "-")

		// PART 1
		if containsSection(firstPair, secondPair) {
			numOfIdenticalSection++
		} else if containsSection(secondPair, firstPair) {
			numOfIdenticalSection++
		}

		// PART 2
		if overlapsSection(firstPair, secondPair) {
			numOfOverlappingSection++
		} else if overlapsSection(secondPair, firstPair) {
			numOfOverlappingSection++
		}
	}
	utils.Print("Res (part 1) ->", utils.IntToString(numOfIdenticalSection))
	utils.Print("Res (part 2) ->", utils.IntToString(numOfOverlappingSection))
}
