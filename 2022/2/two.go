package main

import (
	utils "aoc"
)

// PART 1 and PART 2
// 	scoring_system["A"] = 1 // rock
// 	scoring_system["B"] = 2 // paper
// 	scoring_system["C"] = 3 // scissors

// 	scoring_system["X"] = 1 // rock
// 	scoring_system["Y"] = 2 // paper
// 	scoring_system["Z"] = 3 // scissors

// PART 2
// X == Lose
// Y == Draw
// Z == Win

// **NEW WAY**

func calc_points(system map[string]int, data []string) {
	highest_score := 0

	for i := 0; i < len(data)-1; i++ {
		highest_score += system[data[i]]
	}

	utils.Print("Highest score ->", highest_score)
}

func c_part_1(data []string) {
	// **Pre-calculated data**
	scoring_system := make(map[string]int)
	scoring_system["A X"] = 4
	scoring_system["B X"] = 1
	scoring_system["C X"] = 7

	scoring_system["A Y"] = 8
	scoring_system["B Y"] = 5
	scoring_system["C Y"] = 2

	scoring_system["A Z"] = 3
	scoring_system["B Z"] = 9
	scoring_system["C Z"] = 6

	calc_points(scoring_system, data)
}

func c_part_2(data []string) {
	// **Pre-calculated data**
	scoring_system := make(map[string]int)
	scoring_system["A X"] = 3
	scoring_system["B X"] = 1
	scoring_system["C X"] = 2

	scoring_system["A Y"] = 4
	scoring_system["B Y"] = 5
	scoring_system["C Y"] = 6

	scoring_system["A Z"] = 8
	scoring_system["B Z"] = 9
	scoring_system["C Z"] = 7

	calc_points(scoring_system, data)
}

// **OLD WAY**

func calc_outcome(scoring_system map[string]int, me string, enemy string) int {
	// draw
	if scoring_system[me] == scoring_system[enemy] {
		return 3
	}

	//rock
	if me == "X" {
		if enemy == "C" {
			return 6
		} else {
			return 0
		}
	}

	// paper
	if me == "Y" {
		if enemy == "A" {
			return 6
		} else {
			return 0
		}
	}

	// scissors
	if me == "Z" {
		if enemy == "B" {
			return 6
		} else {
			return 0
		}
	}

	return 0 // will never occur
}

func m_part_1(data []string) {
	scoring_system := make(map[string]int)
	scoring_system["A"] = 1 // rock
	scoring_system["B"] = 2 // paper
	scoring_system["C"] = 3 // scissors

	scoring_system["X"] = 1 // rock
	scoring_system["Y"] = 2 // paper
	scoring_system["Z"] = 3 // scissors

	highest_score := 0

	for i := 0; i < len(data)-1; i++ {
		battle := utils.Split(string(data[i]), " ")
		enemy := battle[0]
		me := battle[1]

		sum := scoring_system[me]
		sum += calc_outcome(scoring_system, me, enemy)

		highest_score += sum
	}

	utils.Print("Highest score ->", highest_score)
}

func m_part_2(data []string) {
	// FIXME:
}

func main() {
	strategy_input := utils.ReadFile("./two_input.txt")

	c_part_1(strategy_input)
	c_part_2(strategy_input)

	utils.Print("...")

	m_part_1(strategy_input)
	m_part_2(strategy_input)
}
