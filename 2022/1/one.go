package main

import (
	utils "aoc"
)

func main() {
	calories_data := utils.ReadFile("./one_input.txt")

	// **Finding**
	var sum_of_calories_data []int
	calories := 0

	for i := 0; i < len(calories_data); i++ {
		if calories_data[i] != "" {
			intVar := utils.StringToInt(calories_data[i])
			calories += intVar
		} else {
			sum_of_calories_data = append(sum_of_calories_data, calories)
			calories = 0
		}
	}
	utils.Print("Len ->", len(sum_of_calories_data))

	// **Sorting**
	utils.SortListOfInts(sum_of_calories_data)
	sum := 0

	for i := len(sum_of_calories_data) - 1; i > len(sum_of_calories_data)-4; i-- {
		utils.Print("Calories ->", sum_of_calories_data[i])

		sum += sum_of_calories_data[i]
	}
	utils.Print("All ->", sum)
}
