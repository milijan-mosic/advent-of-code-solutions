package utils

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func ReadFile(path string) []string {
	data, error := os.ReadFile(path)
	Check(error)

	return strings.Split(string(data), "\n")
}

// Descending order
func SortListOfInts(list []int) {
	sort.Ints(list)
}

func StringToInt(char string) int {
	intVar, error := strconv.Atoi(char)
	Check(error)

	return intVar
}

func IntToString(number int) string {
	return strconv.Itoa(number)
}

func Split(text, char string) []string {
	return strings.Split(text, char)
}

func Print(text string, data ...any) {
	fmt.Println(text, data)
}

func ContainsSubstring(str, substr string) bool {
	for i := 0; i < len(str)-len(substr)+1; i++ {
		if str[i:i+len(substr)] == substr {
			return true
		}
	}
	return false
}
